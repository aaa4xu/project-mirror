<?php
/**
* Copyright (C) 2017 Laurent CLOUET
* Author Laurent CLOUET <laurent.clouet@nopnop.net>
**/

require_once(dirname(__FILE__).'/includes/core.inc.php');

use Phyrexia\Http\Client as HttpClient;

function cleanup_files() {
	global $config;
	
	$xml =  (string)HttpClient::get($config['main-website']['url'].'/api.php?action=mirror_sync');
	if (is_string($xml) == false || strlen($xml) == 0) {
		Logger::debug(__method__.' empty string (input: '.serialize($xml).')');
		return false;
	}
	
	$dom = new DomDocument('1.0', 'utf-8');
	$buf = @$dom->loadXML($xml);
	
	if (! $buf) {
		Logger::debug(__method__.' failed to load xml (input: '.serialize($xml).')');
		return false;
	}
	
	if ($dom->hasChildNodes() == false) {
		Logger::debug(__method__.' xml has no child (input: '.serialize($xml).')');
		return false;
	}
	
	$file_nodes = $dom->getElementsByTagName('scene');
	if (is_null($file_nodes) == false) {
		$projects_on_main_server = array();
		foreach ($file_nodes as $a_node) {
			$projects_on_main_server []= $config['storage']['projects'].((int)($a_node->getAttribute('id'))).'.zip';
		}
		
		// loop for local files
		foreach(glob($config['storage']['projects'].'*.zip') as $f) {
			
			if (in_array($f, $projects_on_main_server) == false) {
				echo "should remove $f\n";
				unlink($f);
			}
		}
	}
}

function cleanup_access() {
	global $config;
	
	$limit = time() - $config['access']['life_span'];
	foreach(Access::loadAll() as $access) {
		if ($access->getCreation() < $limit) {
			Logger::debug(__method__.' remove '.$access);
			$access->remove();
		}
	}
}

cleanup_files();
cleanup_access();
