CREATE TABLE IF NOT EXISTS `access` (
    `id` varchar(20) NOT NULL,
    `creation` int(11) NOT NULL,
    `path` text NOT NULL,
    PRIMARY KEY (`id`)
);
