<?php
/**
* Copyright (C) 2017 Laurent CLOUET
* Author Laurent CLOUET <laurent.clouet@nopnop.net>
**/

require_once(dirname(__FILE__).'/includes/core.inc.php');

if (isFromMainServer() == false) {
	Logger::error(__file__.':'.__line__.' request banned because remove_addr is \''.$_SERVER['REMOTE_ADDR'].'\'');
	die();
}

if ($_FILES['archive']['error'] != 0) {
	Logger::error('Failed to upload error: '.$_FILES['archive']['error'].' (upload_max_filesize: '.ini_get('upload_max_filesize').')');
	die();
}

$total = 0;
foreach(glob($config['storage']['projects'].'/*.zip') as $f) {
	$total += filesize($f);
}



if ($config['storage']['max'] > 0 && isset($_FILES['archive']['size'])) {
	// projects file are ordered by id, so the lowest is the oldest
	foreach(glob($config['storage']['projects'].'/*.zip') as $f) {
		if (basename($f, ".zip") != '1') {
			if ($_FILES['archive']['size'] + $total > $config['storage']['max']) {
				// we need to remove some projects before adding the new one
				$size_project = filesize($f);
				
				unlink($f);
				$total -= $size_project;
				Logger::debug('remove '.$f.' '.$size_project.' new total size: '.$total.' max: '.$config['storage']['max']);
			}
			else {
				break;
			}
		}
	}
}

if (array_key_exists('scene', $_REQUEST) == false) {
	Logger::error('No scene id given');
	die();
}

$local_uploaded_file = $_FILES['archive']['tmp_name'];
$destination_zip = $config['storage']['projects'].((int)($_REQUEST['scene'])).'.zip';

$finfo = finfo_open(FILEINFO_MIME_TYPE); // return mime type ala mimetype extension
if ('application/zip' != finfo_file($finfo, $local_uploaded_file)) {
	Logger::debug('File uploaded is not a zip it is : '.finfo_file($finfo, $local_uploaded_file));
	finfo_close($finfo);
	die();
}
finfo_close($finfo);

move_uploaded_file($local_uploaded_file, $destination_zip);
Logger::debug('File uploaded '.$destination_zip);

echo $total + $_FILES['archive']['size'];
