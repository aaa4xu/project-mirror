<?php
$config = array();
$config['storage']['projects'] = '/tmp/projects/';
$config['storage']['max'] = 0; // max size available for storage, in B. 0 -> unlimited

$config['log']['dir'] = '/tmp/logs/';
$config['log']['debug'] = true;
$config['log']['stdout'] = false;

$config['tmp_dir'] = '/tmp';

$config['access']['life_span'] = 3600;

$config['main-website']['remote_addr_authorized'] = array('23.227.161.142', '2604:880:205:70::2', '51.89.41.122', '2001:41d0:700:317a::');
$config['main-website']['url'] = 'https://www.sheepit-renderfarm.com';
