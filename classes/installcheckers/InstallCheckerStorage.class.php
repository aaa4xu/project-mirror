<?php
/**
 * Copyright (C) 2018 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

class InstallCheckerStorage implements InstallChecker {
	public function name() {
		return 'Storage directory';
	}
	
	public function check() {
		global $config;
		
		if (file_exists($config['storage']['projects']) == false) {
			return $this->failed('Storage directory does not exist');
		}
		
		if (is_writable($config['storage']['projects']) == false) {
			return $this->failed('Storage directory is not writable');
		}
		
		$filename = $config['storage']['projects'].'/'.time().'test';
		if (@file_put_contents($filename, 'test') === false) {
			return $this->failed('Can not add a file on storage directory');
		}
		
		@unlink($filename);
		
		return $this->ok();
	}
	
	private function ok() {
		return '<span style="color: green;">OK</span>';
	}
	
	private function failed($msg) {
		return '<span style="color: red;">'.$msg.'</span>';
	}
}
