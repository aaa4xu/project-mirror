<?php
/**
 * Copyright (C) 2017 Laurent CLOUET
 * Author Laurent CLOUET <laurent.clouet@nopnop.net>
 **/

class Access {
	
	/**
	 * @var string
	 */
	private $id;
	
	/**
	 * @var string
	 */
	private $path;
	
	/**
	 * @var string
	 */
	private $creation;


	public function __construct() {
		$this->creation = time();
	}
	
	/**
	 * @return string
	 */
	public function getId() {
		return $this->id;
	}
	
	/**
	 * @param string $id
	 */
	public function setId($id) {
		$this->id = $id;
	}
	
	/**
	 * @return string
	 */
	public function getPath() {
		return $this->path;
	}
	
	/**
	 * @param string $path
	 */
	public function setPath($path) {
		$this->path = $path;
	}
	
	/**
	 * @return string
	 */
	public function getCreation() {
		return $this->creation;
	}
	
	/**
	 * @param string $creation
	 */
	public function setCreation($creation) {
		$this->creation = $creation;
	}
	
	public function __toString() {
		return 'Access(id:'.$this->id.' path: '.$this->path.' creation: '.$this->creation.')';
	}

	public function add() {
		Logger::debug(__method__.' '.$this);
		
		if (is_null($this->getId())) {
			$this->setId(uniqid());
		}
		
		return apcu_store($this->getId(), $this);
	}
	
	public function remove() {
		Logger::debug(__method__.' '.$this);
		return apcu_delete($this->getId());
	}
	
	public static function load($id) {
		$data = apcu_fetch($id);
		if ($data === false) {
			return null;
		}
		
		return $data;
	}
	
	public static function loadAll() {
		$ret = array();
		foreach (new APCUIterator() as $counter) {
			$o = $counter['value'];
			$ret[$o->getId()] = $o;
		}
		return $ret;
	}
}
